package com.xkhadoop.third.sixth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;





//import java.util.Iterator;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import com.xkhadoop.third.second.SecondSort.Pair;


public class OrderBy {
	
	public static class KeyReverse extends WritableComparator{
		protected KeyReverse(){
			super(Text.class,true);
		}
		@SuppressWarnings("rawtypes")
		public int compare(WritableComparable a,WritableComparable b){
			Text a1=(Text)a;
			Text a2=(Text)b;
			return -a1.compareTo(a2);
		}
	}
	
	public static class GroupingComparator extends WritableComparator {  
        protected GroupingComparator() {  
          super(Pair.class, true);  
        }  
        @SuppressWarnings("rawtypes")
		@Override  
        public int compare(WritableComparable w1, WritableComparable w2) {  
          Pair ip1 = (Pair) w1;  
          Pair ip2 = (Pair) w2;  
          LongWritable l = ip1.getFirst();  
          LongWritable r = ip2.getFirst();
          return l.compareTo(r);
          //return l == r ? 0 : (l < r ? -1 : 1);  
        }  
      } 
	
	public static class Map extends Mapper<Object, Text, Text, IntWritable> {
		Text outputKey=null;
		IntWritable outputValue=null;
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String valueString = value.toString();
			String[] items = valueString.split(" ");
			
			outputKey = new Text(items[0]);
			outputValue = new IntWritable(Integer.valueOf(items[1]));
			context.write(outputKey, outputValue);
//			System.out.println("Map is : "+" key1:"+outputKey.toString()+" ****Value1:" +outputValue.toString());

		}
	}

	public static class Reduce extends
			Reducer<Text, IntWritable, Text, IntWritable> {

		public void reduce(Text key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			ArrayList<Integer> al = new ArrayList<Integer>();
			
			for (IntWritable val : values) {
				al.add(val.get());
			}

			Collections.sort(al, new Comparator<Integer>() {
				public int compare(Integer o1, Integer o2) {
					return o1.compareTo(o2);
				}
			});

			for(Integer val: al){
				context.write(key, new IntWritable(val.intValue()));
			}
		}

	}
	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		if (otherArgs.length < 2) {
			System.err.println("Usage: wordcount <in> [<in>...] <out>");
			System.exit(2);
		}
		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "OrderBy");
		job.setJarByClass(OrderBy.class);
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		//job.setNumReduceTasks(3);
		
//		 // 分区函数  
//        job.setPartitionerClass(FirstPartitioner.class);  
//         key jiangxu   
        job.setSortComparatorClass(KeyReverse.class);  
		
		for (int i = 0; i < otherArgs.length - 1; ++i) {
			FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
		}
		FileOutputFormat.setOutputPath(job, new Path(
				otherArgs[otherArgs.length - 1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
